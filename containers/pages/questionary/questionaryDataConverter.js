import { ACCOUNT_BANNER_SEA } from '../../../constants/bannersConstants'

export default (data) => ({
    banner: ACCOUNT_BANNER_SEA,
    isAsideHidden: true,
    hasCancelRequest: data.subscription.has_open_cancellation_request,
})
