import { createSelector } from 'reselect'

const transactionsSelector = state => ({
    transactions: state.transactions,
})

export const transactionsLengthSelector = createSelector(
    [transactionsSelector],
    (transactions) => transactions.length,
)
