import React from 'react'
import { connect } from 'react-redux'
import withProps from 'recompose/withProps'
import { compose } from 'redux'
import HeroComponent from 'react-ug/components/HeroComponent'
import AccountPanelComponent from 'react-ug/components/AccountPanelComponent'
import AccountTableComponent from 'react-ug/components/AccountTableComponent'
import PaymentMethodImageComponent from 'react-ug/components/PaymentMethodImageComponent'
import { transactionsLengthSelector } from './paymentHistorySelectors'

export default compose(
    connect(
        state => ({
            title: state.content.title,
            transactions: state.content.transactions,
            transactionsLength: transactionsLengthSelector(state),
        }),
    ),
    withProps({
        titleText: 'Payments history',
        noPaymentsDescriptionText: 'Purchase History will be available after your first subscription or purchase.',
        columns: [
            'Transaction ID',
            'Date',
            'Price',
            'Description',
        ],
    }),
    withProps(({ transactions }) => ({
        transactionsFormatted: transactions.map(({
            orderNumber, date, price, paymentMethod, description,
        }) => ([
            orderNumber,
            date,
            {
                render: () => (
                    <PaymentMethodImageComponent
                        type={paymentMethod}
                        text={price}
                    />
                ),
            },
            description,
        ])),
    })),
)(
    ({
        titleText,
        noPaymentsDescriptionText,
        columns,
        transactionsFormatted,
        transactionsLength,
    }) => (
        <div>
            <HeroComponent
                noIndent
                title={titleText}
            />
            {!transactionsLength && (
                <AccountPanelComponent isBGTransparent isNoIndent>
                    <AccountPanelComponent.Content hasTopIndent>
                        {noPaymentsDescriptionText}
                    </AccountPanelComponent.Content>
                </AccountPanelComponent>
            )}
            {transactionsLength && (
                <AccountPanelComponent isBGTransparent isNoIndent>
                    <AccountPanelComponent.Content hasTopIndent>
                        <AccountTableComponent
                            columns={columns}
                            rows={transactionsFormatted}
                        />
                    </AccountPanelComponent.Content>
                </AccountPanelComponent>
            )}
        </div>
    ),
)
