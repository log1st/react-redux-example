export default () => Promise.all([
    import(/* webpackChunkName: 'account_page' */ './PaymentHistoryContentContainer'),
    import(/* webpackChunkName: 'account_page' */ './paymentHistoryDataConverter'),
])
