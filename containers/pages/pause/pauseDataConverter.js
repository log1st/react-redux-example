import cookieHelper from '../../../../../../../helpers/cookieHelper'
import { ACCOUNT_BANNER_BENEFITS } from '../../../constants/bannersConstants'

const REASON_TRIAL_TOO_SHORT = 'trial_period_is_not_enough_to_evaluate_the_service'
const REASON_STOPPED_PLAYING = 'stopped_playing_guitar_dont_need_the_service_anymore'

export default (data) => {
    let monthsForPause = 1
    let title
    let text
    
    title = 'BEFORE YOU GO, A FEW WORDS...'
    text = 'Get a month for FREE! With Ultimate Guitar Pro you also get the ad-free access to various features including video lessons and full access to the official mobile apps on all platforms.'

    const reason = cookieHelper.get('_ug_cancelSubscriptionReason')
    if (reason === REASON_TRIAL_TOO_SHORT) {
        title = 'GET FREE 1-MONTH ACCESS'
        text = 'If 7-day free trial was not enough for you, take advantage of our special offer and get an extra 1-month free access to Ultimate Guitar Pro.'
    } else if (reason === REASON_STOPPED_PLAYING) {
        title = 'STOPPED PLAYING GUITAR? YOU DON\'T HAVE TO PAY!'
        text = 'If you don\'t have enough time to play guitar at the moment, we can offer you something special - put your membership on hold for 3 months and use Ultimate Guitar Pro free during this period. This will allow you to keep your favorite songs safe. After 3 months have passed you can come back and decide if you wish continue.'
        monthsForPause = 3
    }

    return {
        banner: ACCOUNT_BANNER_BENEFITS,
        isAsideHidden: true,
        monthsForPause,
        title,
        text,
        hasCancelRequest: data.subscription.has_open_cancellation_request,
    }
}
