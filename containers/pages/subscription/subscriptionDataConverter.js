import { ACCOUNT_BANNER_ROCK } from '../../../constants/bannersConstants'

export default data => ({
    banner: ACCOUNT_BANNER_ROCK,
    hasAutoRenew: data.has_autorenew,
    hasCancelRequest: data.subscription.has_open_cancellation_request,
    isSubscriptionOnPause: data.already_paused,
    subscription: {
        paymentMethod: data.method,
        cardNumber: data.card_number,
        nextTransactionDate: data.next_transaction_date,
        nextTransactionPrice: data.next_transaction_price,
    },
})

