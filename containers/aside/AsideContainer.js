import { connect } from 'react-redux'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import AccountMenuComponent from 'react-ug/components/AccountMenuComponent'
import { getLogoutUrl } from '../../../../helpers/url'
import { LABEL_SIDEBAR } from '../../constants/eventsConstants'
import { changePaymentMethod } from '../../actions/accountActions'

export default compose(
    connect(
        store => ({
            hasProLifetime: store.user.access.hasProLifetime,
            avatar: store.user.avatarUrl,
            username: store.user.username.text,
        }),
        {
            onPaymentMethodClick: changePaymentMethod(LABEL_SIDEBAR),
        },
    ),
    withProps(({ hasProLifetime, onPaymentMethodClick }) => ({
        items: [
            {
                key: 'index',
                title: 'Account overview',
                url: '/pro/account',
            },
            {
                key: 'password',
                title: 'Change password',
                url: '/forum/profile/update-password',
            },
            {
                key: 'mobile',
                title: 'Mobile access',
                url: '/pro/account/mobile-access',
            },
            hasProLifetime ? false : {
                key: 'payment',
                title: 'Payment method',
                onClick: onPaymentMethodClick,
            },
            {
                key: 'subscription',
                title: 'Subscription',
                url: '/pro/account/subscription',
            },
            {
                key: 'history',
                title: 'Payments history',
                url: '/pro/account/payment-history',
            },
            {
                key: 'logout',
                title: 'Logout',
                url: getLogoutUrl({ redirect: '/' }),
            },
        ].filter(item => item)
    })),
)(AccountMenuComponent)
