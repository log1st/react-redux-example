import React from 'react'
import compose from 'recompose/compose'
import HeroComponent from 'react-ug/components/HeroComponent'
import AccountPanelComponent from 'react-ug/components/AccountPanelComponent'
import withProps from 'recompose/withProps'
import withState from 'recompose/withState'
import lifecycle from 'recompose/lifecycle'
import AccountQuestionnaireComponent from 'react-ug/components/AccountQuestionnaireComponent'
import ControlListComponent from 'react-ug/components/ControlListComponent'
import { BUTTON_TYPE_LINK } from 'react-ug/constants/buttonTypesContants'
import { BUTTON_STATE_LINK_SUCCESS, BUTTON_STATE_LINK_THIRD } from 'react-ug/constants/buttonStatesContants'
import { BUTTON_SIZE_MD } from 'react-ug/constants/buttonSizesContants'
import { CONTROL_LIST_INDENT_XL } from 'react-ug/constants/controlListIndentsConstants'
import { connect } from 'react-redux'
import { ANSWER_TYPE_RADIOBUTTON } from 'react-ug/constants/accountQuestionaryAnswerTypesConstants'
import { trackEvent } from '../../../actions/accountActions'
import {
    ACTION_CANCEL_REASON,
    CATEGORY_ACCOUNT,
} from '../../../constants/eventsConstants'
import cookieHelper from '../../../../../../../helpers/cookieHelper'
import { navigateToUrl } from '../../../../../../../helpers/urlHelper'

export default compose(
    connect(
        store => ({
            choices: store.content.choises,
            isTrialActive: store.user.access.isTrialActive,
            hasProLifetime: store.user.access.hasProLifetime,
            hasCancelRequest: store.content.hasCancelRequest,
        }),
    ),
    withProps({
        titleText: 'We will do anything to solve your problem',
        descriptionText: 'Please select the reason, why you wish to turn off automatic subscription renewal.',
    }),
    withState('reason', 'setReason', null),
    withProps(({
        reason,
    }) => ({
        onContinueClick: () => {
            if (reason) {
                trackEvent(
                    CATEGORY_ACCOUNT,
                    ACTION_CANCEL_REASON,
                    null,
                    {
                        reason,
                    },
                )
                cookieHelper.set('_ug_cancelSubscriptionReason', reason, 600)
            }
        },
    })),
    lifecycle({
        componentDidMount() {
            const {
                hasProLifetime,
                hasCancelRequest,
            } = this.props

            if (hasProLifetime || hasCancelRequest) {
                navigateToUrl('/pro/account')
            }
        },
    }),
    withProps(({ onContinueClick, isTrialActive, reason }) => ({
        answers: [
            isTrialActive ? 'Trial period is not enough to evaluate the service' : 'Stopped playing guitar - don\'t need the service anymore',
            'Playback issues/bugs',
            "Couldn't find the song I was looking for",
            "I'm a beginner, it's all too complicated for me/just need the chords",
            'Low sound/tab quality',
            'I play another instrument/I need sheet music',
            'Found a better way to learn guitar',
            'Other',
        ].map(label => {
            const value = label.toLowerCase()
                .replace(/'/g, '')
                .replace(/[^a-z0-9]/g, ' ')
                .replace(/[\s]+/g, ' ')
                .replace(/[\s]/g, '_')
            return ({
                type: ANSWER_TYPE_RADIOBUTTON,
                text: label,
                value,
                isChecked: reason === value,
            })
        }),
        actions: [
            {
                type: BUTTON_TYPE_LINK,
                state: BUTTON_STATE_LINK_SUCCESS,
                text: 'Stay subscribed',
                url: '/',
                size: BUTTON_SIZE_MD,
            },
            {
                type: BUTTON_TYPE_LINK,
                state: !reason ? BUTTON_STATE_LINK_THIRD : BUTTON_STATE_LINK_SUCCESS,
                text: 'Continue',
                disabled: !reason,
                url: '/pro/account/pause',
                onClick: onContinueClick,
                size: BUTTON_SIZE_MD,
            },
        ],
    })),
)(
    ({
        titleText,
        descriptionText,
        setReason,
        answers,
        actions,
    }) => (
        <div>
            <HeroComponent
                noIndent
                title={titleText}
            />
            <AccountPanelComponent isBGTransparent isNoIndent>
                <AccountPanelComponent.Content hasTopIndent>
                    {descriptionText}
                </AccountPanelComponent.Content>
            </AccountPanelComponent>
            <AccountPanelComponent isBGTransparent isNoIndent>
                <AccountQuestionnaireComponent onChange={setReason} answers={answers} />
            </AccountPanelComponent>
            <AccountPanelComponent isBGTransparent isNoIndent>
                <ControlListComponent
                    isNoCollapsing
                    collapsingSize={CONTROL_LIST_INDENT_XL}
                    hasNoIndent
                    actions={actions}
                />
            </AccountPanelComponent>
        </div>
    ),
)
