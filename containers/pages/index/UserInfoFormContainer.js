import React from 'react'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import { connect } from 'react-redux'
import FormComponent from 'react-ug/components/FormComponent'
import { INPUT_STATE_LIGHT } from 'react-ug/constants/inputStatesContstants'
import { BUTTON_STATE_SOLID_SUCCESS_BUY } from 'react-ug/constants/buttonStatesContants'
import { saveProfile, saveUserData } from '../../../actions/accountActions'

@compose(
    connect(
        state => ({
            username: state.user.username.text,
            joinedVia: state.content.joinedVia,
            birthday: state.content.birthday,
            joinDate: state.content.joinDate,
            country: state.content.country,
            profile: state.content.profile,
            choices: state.content.profileChoises,
        }),
        {
            onSaveProfile: saveUserData,
        },
    ),
    withProps({
        usernameLabelText: 'Username',
        joinedViaLabelText: 'Joined via',
        birthdayLabelText: 'Date of birth',
        joinDateLabelText: 'Join date',
        countryLabelText: 'Country',
        instrumentLabelText: 'What instrument do you primarily play?',
        chordsSkillLabelText: 'Chord skills',
        tabsSkillLabelText: 'Tabs skills',
        frequencyLabelText: 'How often do you play?',
        experienceLabelText: 'How long have you been playing?',
        buttonSubmitText: 'Save changes',
        notSetPlaceholderText: 'Not set',
    }),
    withProps(({ onSaveProfile, birthday, profile, setIsEditing }) => ({
        onSubmit: (props) => {
            setIsEditing(false)

            const newFields = {
                ...profile,
                birthday,
                ...props,
            }

            let birthdayObject = newFields.birthday
            if (birthdayObject === String(birthdayObject)) {
                const dateParts = birthdayObject.match(/(\d{4})-(\d{2})-(\d{2})/)
                if (dateParts) {
                    birthdayObject = {
                        year: dateParts[1],
                        month: dateParts[2],
                        day: dateParts[3],
                    }
                }
            }
            newFields.birthday = birthdayObject

            saveProfile(newFields)
            onSaveProfile(newFields)
        },
    })),
)
export default class UserInfoFormContainer extends FormComponent {
    disableSubmit() {

    }

    get rules() {
        return [

        ]
    }

    get submitButtonState() {
        return BUTTON_STATE_SOLID_SUCCESS_BUY
    }

    get submitButtonText() {
        const {
            isEditing,
            buttonSubmitText,
        } = this.props

        return isEditing ? buttonSubmitText : false
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.isEditing !== this.props.isEditing
    }

    get children() {
        const {
            isEditing,

            usernameLabelText, username,
            joinedViaLabelText, joinedVia,
            birthdayLabelText, birthday,
            joinDateLabelText, joinDate,
            countryLabelText, country,

            instrumentLabelText,
            chordsSkillLabelText,
            tabsSkillLabelText,
            frequencyLabelText,
            experienceLabelText,

            profile,
            choices,
            notSetPlaceholderText,
        } = this.props

        const { year, month, day } = birthday

        const birthdayString = [year, month, day].filter(item => item.length).length ? [year, month, day].join('-') : notSetPlaceholderText

        const {
            instrument,
            chords_skill: chordsSkill,
            tabs_skill: tabsSkill,
            frequency,
            experience,
        } = profile

        const formattedChoices = {}

        Object.keys(choices).map(key => {
            formattedChoices[key] = choices[key].map((item, i) => ({
                value: i,
                title: item,
            }))
            return key
        })

        const {
            instrument: instrumentChoices,
            chords_skill: chordsSkillChoices,
            tabs_skill: tabsSkillChoices,
            frequency: frequencyChoices,
            experience: experienceChoices,
        } = formattedChoices

        return (
            <FormComponent.Row>
                <FormComponent.Column>
                    <FormComponent.Input
                        state={INPUT_STATE_LIGHT}
                        label={usernameLabelText}
                        hidden
                    >
                        {username}
                    </FormComponent.Input>
                    <FormComponent.Input
                        state={INPUT_STATE_LIGHT}
                        label={joinedViaLabelText}
                        hidden
                    >
                        {joinedVia.type + (joinedVia.type === 'email' ? ` (${joinedVia.email})` : '')}
                    </FormComponent.Input>
                    <FormComponent.Input
                        type="date"
                        state={INPUT_STATE_LIGHT}
                        label={birthdayLabelText}
                        defaultValue={birthdayString}
                        name="birthday"
                        hidden={!isEditing}
                        hasNoMargin
                        onChange={this.onInputChange}
                    >
                        {!isEditing && birthdayString}
                    </FormComponent.Input>
                    <FormComponent.Input
                        state={INPUT_STATE_LIGHT}
                        label={joinDateLabelText}
                        hidden
                    >
                        {joinDate || notSetPlaceholderText}
                    </FormComponent.Input>
                    <FormComponent.Input
                        state={INPUT_STATE_LIGHT}
                        label={countryLabelText}
                        hidden
                    >
                        {country || notSetPlaceholderText}
                    </FormComponent.Input>
                </FormComponent.Column>
                <FormComponent.Column>
                    <FormComponent.Select
                        state={INPUT_STATE_LIGHT}
                        label={instrumentLabelText}
                        hidden={!isEditing}
                        options={instrumentChoices}
                        defaultValue={+instrument}
                        name="instrument"
                        hasNoMargin
                        onChange={this.onInputChange}
                    >
                        {!isEditing && (
                            instrumentChoices[instrument].title || notSetPlaceholderText
                        )}
                    </FormComponent.Select>
                    <FormComponent.Select
                        state={INPUT_STATE_LIGHT}
                        label={chordsSkillLabelText}
                        hidden={!isEditing}
                        options={chordsSkillChoices}
                        defaultValue={+chordsSkill}
                        name="chords_skill"
                        hasNoMargin
                        onChange={this.onInputChange}
                    >
                        {!isEditing && (
                            chordsSkillChoices[chordsSkill].title || notSetPlaceholderText
                        )}
                    </FormComponent.Select>
                    <FormComponent.Select
                        state={INPUT_STATE_LIGHT}
                        label={tabsSkillLabelText}
                        hidden={!isEditing}
                        options={tabsSkillChoices}
                        defaultValue={+tabsSkill}
                        name="tabs_skill"
                        hasNoMargin
                        onChange={this.onInputChange}
                    >
                        {!isEditing && (
                            tabsSkillChoices[tabsSkill].title || notSetPlaceholderText
                        )}
                    </FormComponent.Select>
                    <FormComponent.Select
                        state={INPUT_STATE_LIGHT}
                        label={frequencyLabelText}
                        hidden={!isEditing}
                        options={frequencyChoices}
                        defaultValue={+frequency}
                        name="frequency"
                        hasNoMargin
                        onChange={this.onInputChange}
                    >
                        {!isEditing && (
                            frequencyChoices[frequency].title || notSetPlaceholderText
                        )}
                    </FormComponent.Select>
                    <FormComponent.Select
                        state={INPUT_STATE_LIGHT}
                        label={experienceLabelText}
                        hidden={!isEditing}
                        options={experienceChoices}
                        defaultValue={+experience}
                        name="experience"
                        hasNoMargin
                        onChange={this.onInputChange}
                    >
                        {!isEditing && (

                            experienceChoices[experience].title || notSetPlaceholderText
                        )}
                    </FormComponent.Select>
                </FormComponent.Column>
            </FormComponent.Row>
        )
    }
}
