export default () => Promise.all([
    import(/* webpackChunkName: 'account_page' */ './PauseContentContainer'),
    import(/* webpackChunkName: 'account_page' */ './pauseDataConverter'),
])
