import apiHelper from '../../../../../helpers/apiHelper'
import {
    LABEL_CANCEL,
    LABEL_INDEX,
    LABEL_MANAGE_SUBSCRIPTION,
    LABEL_MOBILE_ACCESS, LABEL_PAUSE, LABEL_QUESTIONARY,
    LABEL_SUBSCRIPTION,
} from '../constants/eventsConstants'
import {
    TYPE_ACCOUNT_CONFIRM,
    TYPE_ACCOUNT_INDEX,
    TYPE_ACCOUNT_MANAGE_SUBSCRIPTION,
    TYPE_ACCOUNT_MOBILE_ACCESS, TYPE_ACCOUNT_PAUSE, TYPE_ACCOUNT_QUESTIONARY,
    TYPE_ACCOUNT_SUBSCRIPTION,
} from '../getLoader'
import { trackAnalyticsEventByUrl } from '../../../../../helpers/track'
import { mainServer } from '../../../helpers/url'

export const getPeriodText = (period) => {
    let periodText = ''
    if (period === '1_month') {
        periodText = 'month'
    } else if (period === '12_month') {
        periodText = 'year'
    }
    return periodText
}


export default {
    save(payload) {
        apiHelper.post({
            url: '/pro/account/save-profile',
            payload,
        })
    },
    getVisitEventLabelByType(type) {
        return {
            [TYPE_ACCOUNT_INDEX]: LABEL_INDEX,
            [TYPE_ACCOUNT_SUBSCRIPTION]: LABEL_SUBSCRIPTION,
            [TYPE_ACCOUNT_MOBILE_ACCESS]: LABEL_MOBILE_ACCESS,
            [TYPE_ACCOUNT_MANAGE_SUBSCRIPTION]: LABEL_MANAGE_SUBSCRIPTION,
            [TYPE_ACCOUNT_QUESTIONARY]: LABEL_QUESTIONARY,
            [TYPE_ACCOUNT_PAUSE]: LABEL_PAUSE,
            [TYPE_ACCOUNT_CONFIRM]: LABEL_CANCEL,

        }[type] || 'Unknown'
    },
    trackEvent(params) {
        trackAnalyticsEventByUrl({
            url: `${mainServer}/analytics/add-visitor-event`,
            params,
        })
    },
}
