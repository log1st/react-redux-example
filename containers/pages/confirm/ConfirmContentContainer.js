import React from 'react'
import { connect } from 'react-redux'
import BBCode from '@bbob/react/es/Component'
import reactPreset from '@bbob/preset-react/es'
import { BUTTON_STATE_LINK_SUCCESS, BUTTON_STATE_LINK_THIRD } from 'react-ug/constants/buttonStatesContants'
import { BUTTON_SIZE_LG, BUTTON_SIZE_MD } from 'react-ug/constants/buttonSizesContants'
import { BUTTON_TYPE_LINK } from 'react-ug/constants/buttonTypesContants'
import { CONTROL_LIST_INDENT_XL } from 'react-ug/constants/controlListIndentsConstants'
import AccountPanelComponent from 'react-ug/components/AccountPanelComponent'
import withProps from 'recompose/withProps'
import withState from 'recompose/withState'
import lifecycle from 'recompose/lifecycle'
import HeroComponent from 'react-ug/components/HeroComponent'
import ButtonElement from 'react-ug/components/publicElements/ButtonElement'
import { compose } from 'redux'
import ControlListComponent from 'react-ug/components/ControlListComponent'
import { sendCancellationRequest, trackEvent } from '../../../actions/accountActions'
import {
    ACTION_CANCEL_REQUEST,
    CATEGORY_ACCOUNT,
} from '../../../constants/eventsConstants'
import { navigateToUrl } from '../../../../../../../helpers/urlHelper'

const MainPageLink = ({ href, children }) => (
    <ButtonElement
        state={BUTTON_STATE_LINK_SUCCESS}
        size={BUTTON_SIZE_LG}
        type={BUTTON_TYPE_LINK}
        url={href}
        text={Array.from(children).join('')}
    />
)

const preset = reactPreset.extend(tags => ({
    ...tags,
    url: (...args) => ({
        ...tags.url(...args),
        tag: MainPageLink,
    }),
}))

const plugins = [preset()]

export default compose(
    connect(
        state => ({
            username: state.user.username,
            email: state.user.email,
            hasProLifetime: state.user.access.hasProLifetime,
            hasCancelRequest: state.content.hasCancelRequest,
        }),
    ),
    withState('isRequested', 'setIsRequested', false),
    withState('isProcessing', 'setIsProcessing', false),
    withState('error', 'setError', null),
    withProps({
        titleText: 'Turn off automatic renewal',
        descriptionText: 'You can come back at any moment. Just log in with your username and password on [url="/"]ultimate-guitar.com[/url] and start a new subscription.',
    }),
    withProps(({ isProcessing, setIsProcessing, setError, setIsRequested }) => ({
        onSendRequestClick: () => {
            if (isProcessing) {
                return
            }
            setIsProcessing(true)
            setError(null)
            trackEvent(
                CATEGORY_ACCOUNT,
                ACTION_CANCEL_REQUEST,
            )

            sendCancellationRequest()
                .then(() => {
                    setIsRequested(true)
                    setIsProcessing(false)
                })
                .catch(error => {
                    setIsProcessing(false)
                    setError(error)
                })
        },
    })),
    lifecycle({
        componentDidMount() {
            const {
                hasProLifetime,
                hasCancelRequest,
            } = this.props

            if (hasProLifetime || hasCancelRequest) {
                navigateToUrl('/pro/account')
            }
        },
    }),
    withProps(({ email, isProcessing, onSendRequestClick }) => ({
        requestedDescriptionText: `We will reply you within a few hours to your ${email}. Don't worry, no charges would be applied during this period.`,
        actions: [
            {
                type: BUTTON_TYPE_LINK,
                state: BUTTON_STATE_LINK_SUCCESS,
                text: 'Stay subscribed',
                url: '/',
                size: BUTTON_SIZE_MD,
            },
            {
                isLoading: isProcessing,
                type: BUTTON_TYPE_LINK,
                state: isProcessing ? BUTTON_STATE_LINK_THIRD : BUTTON_STATE_LINK_SUCCESS,
                text: isProcessing ? 'Loading...' : 'Send cancellation request',
                size: BUTTON_SIZE_MD,
                onClick: onSendRequestClick,
            },
        ],
    })),
)(
    ({
        titleText,
        requestedDescriptionText,
        descriptionText,
        isRequested,
        error,
        actions,
    }) => (
        <div>
            <HeroComponent
                noIndent
                title={titleText}
            />
            {isRequested && (
                <AccountPanelComponent.Content hasTopIndent>
                    {requestedDescriptionText}
                </AccountPanelComponent.Content>
            )}
            {!isRequested && (
                <div>
                    <AccountPanelComponent isBGTransparent isNoIndent>
                        <AccountPanelComponent.Content hasTopIndent>
                            <BBCode plugins={plugins}>{descriptionText}</BBCode>
                        </AccountPanelComponent.Content>
                        {error && (
                            <AccountPanelComponent.Content hasTopIndent>
                                {error}
                            </AccountPanelComponent.Content>
                        )}
                    </AccountPanelComponent>
                    <AccountPanelComponent isBGTransparent isNoIndent>
                        <ControlListComponent
                            isNoCollapsing
                            collapsingSize={CONTROL_LIST_INDENT_XL}
                            hasNoIndent
                            actions={actions}
                        />
                    </AccountPanelComponent>
                </div>
            )}
        </div>
    ),
)
