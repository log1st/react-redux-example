import React from 'react'
import { connect } from 'react-redux'
import compose from 'recompose/compose'
import AccountPanelComponent from 'react-ug/components/AccountPanelComponent'
import withProps from 'recompose/withProps'
import withState from 'recompose/withState'
import lifecycle from 'recompose/lifecycle'
import AccountQuestionnaireComponent from 'react-ug/components/AccountQuestionnaireComponent'
import ControlListComponent from 'react-ug/components/ControlListComponent'
import { BUTTON_TYPE_LINK } from 'react-ug/constants/buttonTypesContants'
import { BUTTON_STATE_LINK_SUCCESS, BUTTON_STATE_LINK_THIRD } from 'react-ug/constants/buttonStatesContants'
import { BUTTON_SIZE_MD } from 'react-ug/constants/buttonSizesContants'
import { CONTROL_LIST_INDENT_XL } from 'react-ug/constants/controlListIndentsConstants'
import HeroComponent from 'react-ug/components/HeroComponent'
import { ANSWER_TYPE_RADIOBUTTON } from 'react-ug/constants/accountQuestionaryAnswerTypesConstants'
import { switchToSubscription, trackEvent } from '../../../actions/accountActions'
import {
    PRICE_KEY_MONTH,
    PRICE_KEY_YEAR,
} from '../../../../../../../modules/buy/constants/priceKeyConstants'
import buyParamsService from '../../../../../services/buy/buyParamsService'
import {
    ACTION_CLICK_TURN_OFF_AUTORENEWAL,
    ACTION_DOWNGRADE_PLAN,
    ACTION_SWITCH_TO_SUBSCRIPTION_FAILURE,
    ACTION_SWITCH_TO_SUBSCRIPTION_SUCCESS,
    ACTION_UPGRADE_PLAN,
    CATEGORY_ACCOUNT,
} from '../../../constants/eventsConstants'
import { BUY_PERIOD_MONTH, BUY_PERIOD_YEAR } from '../../../../../../../modules/buy/constants/buyPeriodConstants'
import buyService from '../../../../../../../modules/buy/services/buyService'
import { navigateToUrl } from '../../../../../../../helpers/urlHelper'
import { getPeriodText } from '../../../services/accountService'

export default compose(
    connect(
        state => ({
            currency: state.buy.currency,
            subscription: state.content.subscription,
            currentPeriod: state.user.access.proPaymentPeriod,
            hasProLifetime: state.user.access.hasProLifetime,
            hasCancelRequest: state.content.hasCancelRequest,
        }),
    ),
    withState('isProcessing', 'setIsProcessing', false),
    withState('error', 'setError', null),
    withState('plan', 'setPlan', null),
    withProps(({ currentPeriod }) => ({
        currentPeriodText: getPeriodText(currentPeriod),
    })),
    withProps({
        titleText: 'Manage subscription',
        descriptionText: 'Here you can manage your subscription',
    }),
    lifecycle({
        componentDidMount() {
            const {
                hasProLifetime,
                hasCancelRequest,
            } = this.props

            if (hasProLifetime || hasCancelRequest) {
                navigateToUrl('/pro/account')
            }
        },
    }),
    withProps(({
        setPlan,
        subscription,
        plan: oldPlan,
        isProcessing,
        setError,
        setIsProcessing,
        currency,
    }) => ({
        onChangePlan: (plan) => {
            setPlan(plan)
            if (isProcessing) {
                setPlan(oldPlan)
                return
            }

            if (plan === 'cancel' && !subscription.nextTransactionPrice) {
                setPlan('nothingToCancel')
            } else {
                setPlan(plan)
            }
        },
        switchSubscription: (plan) => {
            setIsProcessing(true)
            setError(false)

            const paymentProps = buyService.getCommonPaymentProps({
                currency,
                priceKey: plan === 'downgrade' ? PRICE_KEY_MONTH : PRICE_KEY_YEAR,
                paymentPeriod: plan === 'downgrade' ? BUY_PERIOD_MONTH : BUY_PERIOD_YEAR,
            })


            switchToSubscription(paymentProps)
                .then(() => {
                    trackEvent(
                        CATEGORY_ACCOUNT,
                        ACTION_SWITCH_TO_SUBSCRIPTION_SUCCESS,
                    )
                    navigateToUrl('/pro/account/subscription')
                })
                .catch(error => {
                    setIsProcessing(false)
                    setError(error)

                    trackEvent(
                        CATEGORY_ACCOUNT,
                        ACTION_SWITCH_TO_SUBSCRIPTION_FAILURE,
                        null,
                        {
                            props: paymentProps,
                            error,
                        },
                    )
                })
        },
    })),
    withProps(({
        isProcessing,
        plan,
        switchSubscription,
        currentPeriod,
        currentPeriodText,
        subscription,
        currency,
    }) => ({
        answers: [
            {
                type: ANSWER_TYPE_RADIOBUTTON,
                text: `Your current subscription plan. Premium full subscription ${subscription.nextTransactionPrice ? ` at ${subscription.nextTransactionPrice}/${currentPeriodText}` : ''}`,
                value: 'cancel',
                isChecked: plan === 'cancel',
            },
            currentPeriod === BUY_PERIOD_MONTH ? {
                type: ANSWER_TYPE_RADIOBUTTON,
                text: `Switch from monthly access to annual access for ${buyParamsService.getPrice({
                    priceKey: PRICE_KEY_YEAR,
                    currency,
                })}/year and save 60%`,
                value: 'upgrade',
                isChecked: plan === 'upgrade',
            } : null,
            currentPeriod === BUY_PERIOD_YEAR ? {
                type: ANSWER_TYPE_RADIOBUTTON,
                text: `Switch from annual access to monthly access for ${buyParamsService.getPrice({
                    priceKey: PRICE_KEY_MONTH,
                    currency,
                })}/month`,
                value: 'downgrade',
                isChecked: plan === 'downgrade',
            } : null,
        ].filter(item => item),
        actions: [
            {
                type: BUTTON_TYPE_LINK,
                state: BUTTON_STATE_LINK_SUCCESS,
                text: 'Continue with current plan',
                url: '/',
                size: BUTTON_SIZE_MD,
            },
            plan === 'cancel' ? {
                type: BUTTON_TYPE_LINK,
                state: BUTTON_STATE_LINK_SUCCESS,
                text: 'Turn off auto-renewal',
                url: '/pro/account/questionary',
                onClick: () => {
                    trackEvent(
                        CATEGORY_ACCOUNT,
                        ACTION_CLICK_TURN_OFF_AUTORENEWAL,
                    )
                },
            } : null,
            ['upgrade', 'downgrade'].indexOf(plan) > -1 ? {
                isLoading: isProcessing,
                // disabled: isProcessing,
                type: BUTTON_TYPE_LINK,
                state: isProcessing ? BUTTON_STATE_LINK_THIRD : BUTTON_STATE_LINK_SUCCESS,
                text: isProcessing ? 'Loading...' : ({
                    upgrade: 'Upgrade to annual plan',
                    downgrade: 'Downgrade to a monthly plan',
                }[plan]),
                onClick: (e) => {
                    e.preventDefault()
                    if (isProcessing) {
                        return
                    }
                    trackEvent(
                        CATEGORY_ACCOUNT,
                        plan === 'upgrade' ? ACTION_UPGRADE_PLAN : ACTION_DOWNGRADE_PLAN,
                    )
                    switchSubscription(plan)
                },
            } : null,
        ].filter(item => item),
    })),
)(
    ({
        titleText,
        descriptionText,
        answers,
        error,
        plan,
        actions,
        onChangePlan,
    }) => (
        <div>
            <HeroComponent
                noIndent
                title={titleText}
            />
            <AccountPanelComponent isBGTransparent isNoIndent>
                <AccountPanelComponent.Content hasTopIndent>
                    {descriptionText}
                </AccountPanelComponent.Content>
            </AccountPanelComponent>
            <AccountPanelComponent isBGTransparent isNoIndent>
                <AccountQuestionnaireComponent onChange={onChangePlan} answers={answers} />
                {error && (
                    <AccountPanelComponent.Content hasTopIndent>
                        {error}
                    </AccountPanelComponent.Content>
                )}
            </AccountPanelComponent>
            {plan && (
                <AccountPanelComponent isBGTransparent isNoIndent>
                    <ControlListComponent
                        isNoCollapsing
                        collapsingSize={CONTROL_LIST_INDENT_XL}
                        hasNoIndent
                        actions={actions}
                    />
                </AccountPanelComponent>
            )}
        </div>
    ),
)
