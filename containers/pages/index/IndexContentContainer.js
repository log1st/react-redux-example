import React from 'react'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import { connect } from 'react-redux'
import HeroComponent from 'react-ug/components/HeroComponent'
import AccountPanelComponent from 'react-ug/components/AccountPanelComponent'
import ControlListComponent from 'react-ug/components/ControlListComponent'
import withState from 'recompose/withState'
import { BUTTON_STATE_SOLID_SUCCESS_BUY } from 'react-ug/constants/buttonStatesContants'
import { BUTTON_SIZE_MD } from 'react-ug/constants/buttonSizesContants'
import { BUTTON_TYPE_LINK } from 'react-ug/constants/buttonTypesContants'
import { changePaymentMethod } from '../../../actions/accountActions'
import { LABEL_INDEX } from '../../../constants/eventsConstants'
import UserInfoFormContainer from './UserInfoFormContainer'

export default compose(
    withState('isEditing', 'setIsEditing', false),
    connect(
        store => ({
            accessType: store.user.access.accessType,
            hasProLifetime: store.user.access.hasProLifetime,
        }),
        {
            onUpdatePaymentsDetailsClick: changePaymentMethod(LABEL_INDEX),
        },
    ),
    withProps(({
        onUpdatePaymentsDetailsClick,
    }) => ({
        titleText: 'Account overview',

        profileTitleText: 'Profile',

        paymentDetailsTitleText: 'Payment details',
        paymentDetailsDescriptionText: 'Your Ultimate Guitar Pro password is valid for all Ultimate Guitar services.\n To restore password, click on the button below.',
        paymentDetailsActions: [
            {
                state: BUTTON_STATE_SOLID_SUCCESS_BUY,
                text: 'Update payment details',
                size: BUTTON_SIZE_MD,
                onClick: onUpdatePaymentsDetailsClick,
            },
        ],

        restorePasswordTitleText: 'Restore password',
        restorePasswordDescriptionText: 'Your Ultimate Guitar Pro password is valid for all Ultimate Guitar services. To restore password, click on the button below.',
        restorePasswordActions: [
            {
                state: BUTTON_STATE_SOLID_SUCCESS_BUY,
                text: 'Restore password',
                size: BUTTON_SIZE_MD,
                type: BUTTON_TYPE_LINK,
                url: '/forum/profile/update-password',
            },
        ],
    })),
)(({
    titleText,
    accessType,
    hasProLifetime,

    isEditing,
    setIsEditing,

    profileTitleText,

    paymentDetailsTitleText,
    paymentDetailsDescriptionText,
    paymentDetailsActions,

    restorePasswordTitleText,
    restorePasswordDescriptionText,
    restorePasswordActions,
}) => (
    <div>
        <HeroComponent
            noIndent
            title={titleText}
            primarySubTitle={accessType}
        />
        <AccountPanelComponent title={profileTitleText}>
            <UserInfoFormContainer setIsEditing={setIsEditing} isEditing={isEditing} />
            {!isEditing && (
                <ControlListComponent hasNoIndent actions={[
                    {
                        state: BUTTON_STATE_SOLID_SUCCESS_BUY,
                        text: 'Edit profile',
                        size: BUTTON_SIZE_MD,
                        onClick: () => setIsEditing(true),
                    },
                ]}
                />
            )}
        </AccountPanelComponent>
        {!hasProLifetime && (
            <AccountPanelComponent title={paymentDetailsTitleText}>
                <AccountPanelComponent.Content>
                    {paymentDetailsDescriptionText}
                </AccountPanelComponent.Content>
                <AccountPanelComponent.Content>
                    <ControlListComponent hasNoIndent actions={paymentDetailsActions} />
                </AccountPanelComponent.Content>
            </AccountPanelComponent>
        )}
        <AccountPanelComponent title={restorePasswordTitleText}>
            <AccountPanelComponent.Content>
                {restorePasswordDescriptionText}
            </AccountPanelComponent.Content>
            <AccountPanelComponent.Content>
                <ControlListComponent hasNoIndent actions={restorePasswordActions} />
            </AccountPanelComponent.Content>
        </AccountPanelComponent>
    </div>
))
