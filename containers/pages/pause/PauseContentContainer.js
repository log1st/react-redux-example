import React from 'react'
import { connect } from 'react-redux'
import HeroComponent from 'react-ug/components/HeroComponent'
import AccountPanelComponent from 'react-ug/components/AccountPanelComponent'
import { BUTTON_TYPE_LINK } from 'react-ug/constants/buttonTypesContants'
import {
    BUTTON_STATE_LINK_SUCCESS,
    BUTTON_STATE_LINK_THIRD,
    BUTTON_STATE_SOLID_SUCCESS_BUY,
} from 'react-ug/constants/buttonStatesContants'
import { BUTTON_SIZE_MD } from 'react-ug/constants/buttonSizesContants'
import ControlListComponent from 'react-ug/components/ControlListComponent'
import withProps from 'recompose/withProps'
import withState from 'recompose/withState'
import compose from 'recompose/compose'
import lifecycle from 'recompose/lifecycle'
import { CONTROL_LIST_INDENT_XL } from 'react-ug/constants/controlListIndentsConstants'
import { navigateToUrl } from '../../../../../../../helpers/urlHelper'
import { setSubscriptionOnPause, trackEvent } from '../../../actions/accountActions'
import { ACTION_PAUSE, CATEGORY_ACCOUNT } from '../../../constants/eventsConstants'

export default compose(
    connect(
        store => ({
            monthsForPause: store.content.monthsForPause,
            titleText: store.content.title,
            descriptionText: store.content.text,
            hasProLifetime: store.user.access.hasProLifetime,
            hasCancelRequest: store.content.hasCancelRequest,
        }),
    ),
    withState('error', 'setError', false),
    withState('isProcessing', 'setIsProcessing', false),
    withProps(({ monthsForPause, setError, setIsProcessing }) => ({
        pauseButtonText: `Put membership on hold for ${monthsForPause} month${monthsForPause > 1 ? 's' : ''}`,
        loadingText: 'Loading...',
        onPauseClick: () => {
            trackEvent(
                CATEGORY_ACCOUNT,
                ACTION_PAUSE,
            )

            setError(false)
            setIsProcessing(true)

            setSubscriptionOnPause(monthsForPause)
                .then(() => {
                    navigateToUrl('/pro/account/subscription')
                })
                .catch(error => {
                    setError(error)
                    setIsProcessing(false)
                })
        },
    })),
    lifecycle({
        componentDidMount() {
            const {
                hasProLifetime,
                hasCancelRequest,
            } = this.props

            if (hasProLifetime || hasCancelRequest) {
                navigateToUrl('/pro/account')
            }
        },
    }),
    withProps({
        actions: [
            {
                type: BUTTON_TYPE_LINK,
                state: BUTTON_STATE_LINK_SUCCESS,
                text: 'Stay subscribed',
                url: '/',
            },
            {
                type: BUTTON_TYPE_LINK,
                state: BUTTON_STATE_LINK_SUCCESS,
                text: 'I don\'t need free access',
                url: '/pro/account/confirm',
            },
        ],
    }),
)(
    ({
        titleText,
        descriptionText,
        actions,
        pauseButtonText,
        loadingText,
        isProcessing,
        onPauseClick,
        error,
    }) => (
        <div>
            <HeroComponent
                noIndent
                title={titleText}
            />
            <AccountPanelComponent isBGTransparent isNoIndent>
                <AccountPanelComponent.Content hasTopIndent>
                    {descriptionText}
                </AccountPanelComponent.Content>
            </AccountPanelComponent>
            <AccountPanelComponent isBGTransparent isNoIndent>
                <ControlListComponent
                    hasNoIndent
                    actions={[{
                        state: isProcessing
                            ? BUTTON_STATE_LINK_THIRD
                            : BUTTON_STATE_SOLID_SUCCESS_BUY,
                        text: isProcessing ? loadingText : pauseButtonText,
                        isLoading: isProcessing,
                        size: BUTTON_SIZE_MD,
                        onClick: onPauseClick,
                    }]}
                />
                {error && (
                    <AccountPanelComponent.Content hasTopIndent>
                        {error}
                    </AccountPanelComponent.Content>
                )}
            </AccountPanelComponent>
            {!isProcessing && (
                <AccountPanelComponent isBGTransparent isNoIndent>
                    <ControlListComponent
                        isNoCollapsing
                        collapsingSize={CONTROL_LIST_INDENT_XL}
                        hasNoIndent
                        actions={actions}
                    />
                </AccountPanelComponent>
            )}
        </div>
    ),
)
