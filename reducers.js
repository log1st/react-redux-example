import { createReducer } from '@ug/helpers/redux'
import buy from '../../../../modules/buy/buyReducer'
import page from '../../reducers/pageReducer'
import user from '../../reducers/userReducer'
import auth from '../../containers/auth/authReducer'
import commonContentReducerActions from '../../reducers/page/commonContentReducerActions'
import { ACTION_UPDATE_USER_INFO } from './constants/actionsConstants'

const contentReducer = createReducer({
    actions: {
        ...commonContentReducerActions,
        [ACTION_UPDATE_USER_INFO]: (state, { birthday, ...props }) => ({
            ...state,
            profile: {
                ...state.profile,
                ...props,
            },
            birthday,
        }),
    },
})


export default {
    page,
    buy,
    user,
    auth,
    content: contentReducer,
}
