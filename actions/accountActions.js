import { createAction } from '@ug/helpers/redux'
import {
    ACTION_CLICK_CHANGE_PAYMENT_METHOD,
    CATEGORY_ACCOUNT,
} from '../constants/eventsConstants'
import { BUY_STEP_UPDATE_CARD } from '../../../../../modules/buy/constants/buyStepsConstants'
import { openBuyLayer } from '../../../actions/buy/buyPrimaryActions'
import accountService from '../services/accountService'
import buyService from '../../../../../modules/buy/services/buyService'
import { ACTION_UPDATE_USER_INFO } from '../constants/actionsConstants'
import { navigateToUrl } from '../../../../../helpers/urlHelper'

export const setSubscriptionOnPause = months => buyService.pausePayment(months)

export const switchToSubscription = data => buyService.processExtraPayment(data)

export const sendCancellationRequest = data => buyService.sendCancellationRequest(data)

export const saveUserData = createAction(ACTION_UPDATE_USER_INFO)

export const trackEvent = (category, action, label = null, data = null) => {
    const params = { category, action }
    if (label) { params.label = label }
    if (data) { params.data = JSON.stringify(data) }
    accountService.trackEvent(params)
}

export const changePaymentMethod = (label) => () => dispatch => {
    trackEvent(
        CATEGORY_ACCOUNT,
        ACTION_CLICK_CHANGE_PAYMENT_METHOD,
        label,
    )

    dispatch(openBuyLayer(BUY_STEP_UPDATE_CARD))
}

export const redirect = (url) => {
    navigateToUrl(url)
}

export const saveProfile = ({
    birthday = '',
    instrument = 0,
    chords_skill = 0,
    tabs_skill = 0,
    frequency = 0,
    experience = 0,
}) => {
    accountService.save({
        profile: {
            instrument,
            chords_skill,
            tabs_skill,
            frequency,
            experience,
        },
        birthday,
    })
}
