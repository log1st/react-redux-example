import getAccountIndexContainers from './containers/pages/index/indexLoader'
import getAccountPaymentHistoryContainers from './containers/pages/paymentHistory/paymentHistoryLoader'
import getAccountSubscriptionContainers from './containers/pages/subscription/subscriptionLoader'
import getAccountMobileAccessContainers from './containers/pages/mobileAccess/mobileAccessLoader'
import getAccountManageSubscriptionContainers from './containers/pages/manageSubscription/manageSubscriptionLoader'
import getAccountQuestionaryContainers from './containers/pages/questionary/questionaryLoader'
import getAccountPauseContainers from './containers/pages/pause/pauseLoader'
import getAccountConfirmContainers from './containers/pages/confirm/confirmLoader'

export const TYPE_ACCOUNT_INDEX = 'accountIndex'
export const TYPE_ACCOUNT_PAYMENT_HISTORY = 'accountPaymentHistory'
export const TYPE_ACCOUNT_SUBSCRIPTION = 'accountSubscription'
export const TYPE_ACCOUNT_MOBILE_ACCESS = 'accountMobileAccess'
export const TYPE_ACCOUNT_MANAGE_SUBSCRIPTION = 'accountManageSubscription'
export const TYPE_ACCOUNT_QUESTIONARY = 'accountQuestionary'
export const TYPE_ACCOUNT_PAUSE = 'accountPause'
export const TYPE_ACCOUNT_CONFIRM = 'accountConfirm'

export const getLoaderType = props => {
    const types = {
        'payment-history': TYPE_ACCOUNT_PAYMENT_HISTORY,
        subscription: TYPE_ACCOUNT_SUBSCRIPTION,
        'mobile-access': TYPE_ACCOUNT_MOBILE_ACCESS,
        'manage-subscription': TYPE_ACCOUNT_MANAGE_SUBSCRIPTION,
        questionary: TYPE_ACCOUNT_QUESTIONARY,
        pause: TYPE_ACCOUNT_PAUSE,
        confirm: TYPE_ACCOUNT_CONFIRM,
    }

    return types[props.action] || TYPE_ACCOUNT_INDEX
}

export const pageLoader = {
    [TYPE_ACCOUNT_INDEX]: getAccountIndexContainers,
    [TYPE_ACCOUNT_PAYMENT_HISTORY]: getAccountPaymentHistoryContainers,
    [TYPE_ACCOUNT_SUBSCRIPTION]: getAccountSubscriptionContainers,
    [TYPE_ACCOUNT_MOBILE_ACCESS]: getAccountMobileAccessContainers,
    [TYPE_ACCOUNT_MANAGE_SUBSCRIPTION]: getAccountManageSubscriptionContainers,
    [TYPE_ACCOUNT_QUESTIONARY]: getAccountQuestionaryContainers,
    [TYPE_ACCOUNT_PAUSE]: getAccountPauseContainers,
    [TYPE_ACCOUNT_CONFIRM]: getAccountConfirmContainers,
}
