export default () => Promise.all([
    import(/* webpackChunkName: 'account_page' */ './ManageSubscriptionContentContainer'),
    import(/* webpackChunkName: 'account_page' */ './manageSubscriptionDataConverter'),
])
