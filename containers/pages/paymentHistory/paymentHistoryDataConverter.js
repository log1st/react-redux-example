import { ACCOUNT_BANNER_ROCK } from '../../../constants/bannersConstants'

export default data => {
    const transactions = []
    data.transactions.forEach((t) => {
        const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        let date = new Date(t.date_transaction * 1000)
        date = `${months[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`
        transactions.push({
            orderNumber: t.vindicia_transaction_merchant_id,
            date,
            price: `${Math.round(t.amount * 100) / 100} ${t.price_currency}`,
            paymentMethod: t.payment_method,
            description: t.description,
        })
    })

    return {
        banner: ACCOUNT_BANNER_ROCK,
        isAsideHidden: true,
        transactions,
    }
}
