import React from 'react'
import BBCode from '@bbob/react/es/Component'
import reactPreset from '@bbob/preset-react/es'
import { connect } from 'react-redux'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import { AccountH1Component, AccountH3Component } from 'react-ug/components/AccountTypographyComponent'
import AccountPanelComponent from 'react-ug/components/AccountPanelComponent'
import HeroComponent from 'react-ug/components/HeroComponent'
import {
    ACCOUNT_TYPOGRAPHY_STATE_SUCCESS,
    ACCOUNT_TYPOGRAPHY_STATE_MUTED,
} from 'react-ug/constants/accountTypographyConstants'
import PaymentMethodImageComponent from 'react-ug/components/PaymentMethodImageComponent'
import ControlListComponent from 'react-ug/components/ControlListComponent'
import { BUTTON_STATE_LINK_SUCCESS, BUTTON_STATE_SOLID_SUCCESS_BUY } from 'react-ug/constants/buttonStatesContants'
import { BUTTON_SIZE_LG, BUTTON_SIZE_MD } from 'react-ug/constants/buttonSizesContants'
import { BUTTON_TYPE_LINK } from 'react-ug/constants/buttonTypesContants'
import ButtonElement from 'react-ug/components/publicElements/ButtonElement'
import { LABEL_SUBSCRIPTION } from '../../../constants/eventsConstants'
import { changePaymentMethod } from '../../../actions/accountActions'
import { getDate } from '../../../../../../../helpers/dateHelper'

const ManageSubscriptionLink = ({ href, children }) => (
    <ButtonElement
        state={BUTTON_STATE_LINK_SUCCESS}
        size={BUTTON_SIZE_LG}
        type={BUTTON_TYPE_LINK}
        url={href}
        text={children.join('')}
    />
)

const preset = reactPreset.extend(tags => ({
    ...tags,
    url: (...args) => ({
        ...tags.url(...args),
        tag: ManageSubscriptionLink,
    }),
}))

const plugins = [preset()]

export default compose(
    connect(
        store => ({
            hasAutoRenew: store.content.hasAutoRenew,
            isSubscriptionOnPause: store.content.isSubscriptionOnPause,
            subscription: store.content.subscription,
            hasLifeTime: store.user.access.hasProLifetime,
            accessUntil: store.user.access.accessUntil,
            accessType: store.user.access.accessType,
            hasCancelRequest: store.content.hasCancelRequest,
        }),
        {
            onUpdatePaymentsDetailsClick: changePaymentMethod(LABEL_SUBSCRIPTION),
        },
    ),
    withProps({
        titleText: 'Subscription',
        updatePaymentTitleText: 'Payment method',
        manageSubscriptionText: 'Would you like to [url="/pro/account/manage-subscription"]manage you subscription[/url]?',
    }),
    withProps(({ onUpdatePaymentsDetailsClick, subscription, hasLifeTime, accessUntil }) => ({
        actions: [
            {
                state: BUTTON_STATE_SOLID_SUCCESS_BUY,
                text: 'Update payment method',
                size: BUTTON_SIZE_MD,
                position: 'center',
                onClick: onUpdatePaymentsDetailsClick,
            },
        ],
        text: (() => {
            let text = 'No access'
            if (hasLifeTime) {
                text = 'You’ve got an access to Ultimate Guitar Pro. That’s as cool as it gets :)'
            } else if (subscription.nextTransactionDate) {
                text = `Your subscription will be automatically renewed on ${getDate({ timestamp: subscription.nextTransactionDate * 1000, isRelative: false }) || 'sometime'} and you will be charged ${subscription.nextTransactionPrice}`
            } else {
                text = `Your Pro plan will end on ${getDate({ timestamp: accessUntil, isRelative: false })}`
            }
            return text
        })(),
    })),
)(
    ({
        accessType,
        hasAutoRenew,
        isSubscriptionOnPause,
        subscription,
        titleText,
        updatePaymentTitleText,
        manageSubscriptionText,
        actions,
        hasLifeTime,
        text,
        hasCancelRequest,
    }) => (
        <div>
            <HeroComponent
                noIndent
                title={titleText}
            />
            <AccountPanelComponent>
                <AccountPanelComponent.Content>
                    <AccountH1Component
                        state={ACCOUNT_TYPOGRAPHY_STATE_SUCCESS}
                        isCentered
                        text={accessType}
                    />
                    <AccountH3Component
                        isCentered
                        text={text}
                    />
                </AccountPanelComponent.Content>
                <AccountPanelComponent.Content>
                    <AccountH3Component
                        state={ACCOUNT_TYPOGRAPHY_STATE_MUTED}
                        isDivider
                        text={updatePaymentTitleText}
                    />
                    {subscription.paymentMethod === 'CreditCard' && (
                        <PaymentMethodImageComponent
                            isCentered
                            text={subscription.cardNumber}
                        />
                    )}
                </AccountPanelComponent.Content>
                <AccountPanelComponent.Content>
                    <ControlListComponent hasNoIndent actions={actions} />
                </AccountPanelComponent.Content>
                {!hasCancelRequest && !hasLifeTime && hasAutoRenew && !isSubscriptionOnPause && (
                    <AccountPanelComponent.Content isTextCenter>
                        <BBCode plugins={plugins}>{manageSubscriptionText}</BBCode>
                    </AccountPanelComponent.Content>
                )}
            </AccountPanelComponent>
        </div>
    ),
)
