import { getPeriodText } from '../../../services/accountService'
import { ACCOUNT_BANNER_TREE } from '../../../constants/bannersConstants'

export default data => {
    const plans = {};

    ['downgrade', 'upgrade'].forEach(a => {
        plans[a] = {
            ...data.plans[a],
            periodText: getPeriodText(data.plans[a].period),
        }
    })

    return {
        banner: ACCOUNT_BANNER_TREE,
        isAsideHidden: true,
        hasCancelRequest: data.subscription.has_open_cancellation_request,
        subscription: {
            nextTransactionPrice: data.next_transaction_price,
            plans: Object.keys(data.plans).reduce((result, plan) => {
                // FIXME: @himich - не предал правильный данные
                const props = plans[plan]
                return {
                    ...result,
                    [plan]: {
                        ...props,
                        amount: props.amount || props.price_amount,
                        currency: props.currency || props.price_currency,
                    },
                }
            }, {}),
        },
    }
}
