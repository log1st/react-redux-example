export default () => Promise.all([
    import(/* webpackChunkName: 'account_page' */ './QuestionaryContentContainer'),
    import(/* webpackChunkName: 'account_page' */ './questionaryDataConverter'),
])
