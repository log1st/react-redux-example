import { ACCOUNT_BANNER_ROCK } from '../../../constants/bannersConstants'

export default () => ({
    banner: ACCOUNT_BANNER_ROCK,
    apps: [
        { label: 'iOS app', link: 'https://itunes.apple.com/app/tabs-chords-by-ultimate-guitar/id357828853?mt=8&utm_campaign=Account' },
        { label: 'Android app', link: 'https://play.google.com/store/apps/details?id=com.ultimateguitar.tabs&utm_campaign=Account' },
    ],
})
