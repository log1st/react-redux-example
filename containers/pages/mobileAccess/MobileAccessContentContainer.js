import React from 'react'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import { connect } from 'react-redux'
import HeroComponent from 'react-ug/components/HeroComponent'
import AccountPanelComponent from 'react-ug/components/AccountPanelComponent'
import Image from 'react-ug/components/ImageComponent'
import ControlListComponent from 'react-ug/components/ControlListComponent'
import { BUTTON_STATE_SOLID_SUCCESS_BUY } from 'react-ug/constants/buttonStatesContants'
import { BUTTON_SIZE_MD } from 'react-ug/constants/buttonSizesContants'
import { BUTTON_TYPE_LINK } from 'react-ug/constants/buttonTypesContants'

const image = require('./img/mobile-access.jpg')

export default compose(
    connect(
        store => ({
            apps: store.content.apps,
        }),
    ),
    withProps(({ apps }) => ({
        titleText: 'Mobile access',
        imageAlt: 'Mobile App',
        descriptionTexts: [
            'Subscribers with full access can download Ultimate Guitar\nmobile app for iOS or Android (see links below).',
            'Once you have the app, launch it and log in with your\nusername and password.',
        ],
        appsLinks: apps.map(({ label, link }) => ({
            state: BUTTON_STATE_SOLID_SUCCESS_BUY,
            text: label,
            size: BUTTON_SIZE_MD,
            type: BUTTON_TYPE_LINK,
            url: link,
            position: 'center',
            target: '_blank',
        })),
    })),
)(
    ({ titleText, descriptionTexts, appsLinks, imageAlt }) => (
        <div>
            <HeroComponent noIndent title={titleText} />
            <AccountPanelComponent isBGTransparent isNoIndent>
                {descriptionTexts.map((description, i) => (
                    <AccountPanelComponent.Content hasTopIndent={i === 0}>
                        {description}
                    </AccountPanelComponent.Content>
                ))}
            </AccountPanelComponent>
            <AccountPanelComponent isOverflowHidden isNoIndent>
                <Image url={image} isWide alt={imageAlt} noLazyLoad />
                <AccountPanelComponent.Content hasTopIndent>
                    <AccountPanelComponent.Content hasTopIndent hasBottomIndent>
                        <ControlListComponent hasNoIndent actions={appsLinks} />
                    </AccountPanelComponent.Content>
                </AccountPanelComponent.Content>
            </AccountPanelComponent>
        </div>
    ),
)
