import React from 'react'
import 'react-ug/components/globalStyles.css'
import { connect } from 'react-redux'
import compose from 'recompose/compose'
import withState from 'recompose/withState'
import withProps from 'recompose/withProps'
import lifecycle from 'recompose/lifecycle'
import AccountLayoutComponent from 'react-ug/components/AccountLayoutComponent'
import ThemeProvider, { THEME_LIGHT, THEME_DARK } from 'react-ug/components/ThemeProviderComponent'
import ImageComponent from 'react-ug/components/ImageComponent'
import AccountHeaderComponent from 'react-ug/components/AccountHeaderComponent'
import { pageLoader, getLoaderType } from './getLoader'
import { convertPageData } from '../../actions/pageActions'
import logHelper from '../../../../helpers/logHelper'
import accountService from './services/accountService'
import { trackEvent } from './actions/accountActions'
import { ACTION_VISIT, CATEGORY_ACCOUNT } from './constants/eventsConstants'
import { homeUrl } from '../../helpers/url'
import AsideContainer from './containers/aside/AsideContainer'
import FooterContainer from '../../containers/footer/FooterContainer'
import { AsyncBuyProcessLoaderContainer } from '../../containers/asyncContainers/marketing/asyncBuyProcessContainers'

export default compose(
    withState('PageContainer', 'setPageContainer', 'yes'),
    connect(
        state => ({
            username: state.user.username.text,
            banner: state.content.banner,
            isAsideHidden: state.content.isAsideHidden,
        }),
        { convertPageData },
    ),
    withProps(({ username }) => ({
        links: [
            { text: 'Home', url: '/' },
            { text: 'Help', url: '/components/support/auth/redirect', target: '_blank' },
            { text: username, url: '/pro/account' },
        ],
    })),
    withProps(({ template }) => ({
        loaderType: getLoaderType(template),
    })),
    lifecycle({
        componentDidMount() {
            this.update = this.update.bind(this)
            this.loadByPage()
            trackEvent(
                CATEGORY_ACCOUNT,
                ACTION_VISIT,
                accountService.getVisitEventLabelByType(this.props.loaderType),
                { referrer: document.referrer },
            )
            logHelper.loadErrorLogger()
        },
        loadByPage() {
            pageLoader[this.props.loaderType]().then(this.update).catch(::logHelper.error)
        },
        update(chunks) {
            const [Content, convert] = chunks.map(chunk => chunk.default)
            this.props.setPageContainer(() => Content)
            this.convert = convert

            this.props.convertPageData({
                payload: this.props.data,
                page: this.props.loaderType,
                convert: data => ({ content: convert(data) }),
            })
        },
    }),
)(
    ({ PageContainer, links, banner, isAsideHidden }) => (
        <ThemeProvider theme={THEME_LIGHT}>
            <AccountLayoutComponent
                isFullHeightByFlex
                renderBar={() => (
                    <ThemeProvider theme={THEME_DARK}>
                        <AccountHeaderComponent
                            logoUrl={homeUrl}
                            links={links}
                        />
                    </ThemeProvider>
                )}
                renderAside={() => !isAsideHidden && (
                    <AsideContainer />
                )}
                renderHeader={() => banner && (
                    <ImageComponent
                        url={`/static/public/img/pro/account/banner-${banner}.jpg`}
                        isWide
                        noLazyLoad
                    />
                )}
                renderFooter={() => (
                    <FooterContainer />
                )}
            >
                <PageContainer />
                <AsyncBuyProcessLoaderContainer />
            </AccountLayoutComponent>
        </ThemeProvider>
    ),
)
