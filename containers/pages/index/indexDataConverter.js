import { ACCOUNT_BANNER_ROCK } from '../../../constants/bannersConstants'
import { getDate } from '../../../../../../../helpers/dateHelper'

export default data => {
    return {
        banner: ACCOUNT_BANNER_ROCK,
        country: data.country,
        joinDate: getDate({timestamp: data.date_registration * 1000, isRelative: false}),
        joinedVia: data.joined_via,
        birthday: data.birthday,
        profile: data.profile,
        profileChoises: {
            instrument: ['', 'Acoustic guitar', 'Electric guitar', 'Bass', 'Other'],
            chords_skill: ['', 'Almost none', 'I know about 3-5 chords', 'I know about 10-15 chords', 'I can play arpeggios'],
            tabs_skill: ['', 'Almost none', 'I can play a simple solo', 'I can play regular solos', 'I can play difficult solos'],
            frequency: ['', 'Not playing yet', 'Every day', 'Few times a week', 'Few times a month', 'Once a month or less often'],
            experience: ['', 'Just started', 'Less than a month', 'Few months', 'Less than a year', 'More than a year'],
        },
    }
}
